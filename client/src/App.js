import React from 'react';
import Navbar from './components/Navbar';
// import SalonList from './components/SalonList'
import SalonProfile from './components/SalonProfile';

import './App.css';

function App() {
  return (
    <div className="App">
      <Navbar />
      <div className="content">
        <SalonProfile />
      </div>


    </div>
  );
}

export default App;
