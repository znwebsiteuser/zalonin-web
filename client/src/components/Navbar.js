import React, { Component } from 'react'

export default class Navbar extends Component {
    render() {
        return (
            <div>
                <header>
                    <nav>
                        <div id="navbar">
                            <div id="logo">
                                <h1 className="logo-text">Zalonin</h1>
                            </div>
                            <div id="links">
                                <a href="">Home</a>
                                <a href="">About</a>
                                <a href="">Blog</a>
                                <a href="">Service</a>
                                <a href="">Contact</a>
                            </div>
                        </div>
                    </nav>

                </header>

            </div>
        )
    }
}
