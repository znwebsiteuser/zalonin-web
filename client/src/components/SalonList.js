import React, { Component } from 'react'
import { PageHeader, Tag, Tabs, Button, Statistic, Row, Col } from 'antd';


export default class SalonList extends Component {
    render() {
        return (
            <div>
                <div className="" uk-grid="true">
                    <div className="uk-width-expand@m">
                        <div className="uk-child-width-expand@s" uk-grid="true">
                            <div>
                                <div className="uk-card uk-card-default">
                                    <div className="uk-card-media-top">
                                        <img src="img/post2.jpg" className="image-list" alt="" />
                                    </div>
                                    <div className="uk-card-body padding-box">
                                        <h4 className="uk-card-title">Salon 1</h4>
                                        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                            <li><a href="#">
                                                <span className="location-info">
                                                    NSUT,Dwarka Mor
                                            </span>
                                            </a></li>
                                        </ul>

                                        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                            <li><a href="#">
                                                <i className='uil uil-location-pin-alt ic-size'></i>
                                                <span className="location-info">
                                                    0.23KM
                                            </span>
                                            </a></li>
                                            <li><a href="#">
                                                <i class='uil uil-star ic-size'></i>
                                                <span className="location-info">
                                                    4.2
                                            </span>
                                            </a></li>

                                        </ul>
                                    </div>
                                    <div class="uk-card-footer book-btn">
                                        <a href="#" class="uk-button uk-button-text book-txt">Book Now</a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="uk-card uk-card-default">
                                    <div className="uk-card-media-top">
                                        <img src="img/post6.jpg" alt="" />
                                    </div>
                                    <div className="uk-card-body padding-box">
                                        <h3 className="uk-card-title">Salon 2</h3>
                                        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                            <li><a href="#">
                                                <span className="location-info">
                                                    Noida,UP
                                            </span>
                                            </a></li>
                                        </ul>

                                        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                            <li><a href="#">
                                                <i className='uil uil-location-pin-alt ic-size'></i>
                                                <span className="location-info">
                                                    4.2KM
                                            </span>
                                            </a></li>
                                            <li><a href="#">
                                                <i class='uil uil-star ic-size'></i>
                                                <span className="location-info">
                                                    4.2
                                            </span>
                                            </a></li>

                                        </ul>
                                    </div>
                                    <div class="uk-card-footer book-btn">
                                        <a href="#" class="uk-button uk-button-text book-txt">Book Now</a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="uk-card uk-card-default">
                                    <div className="uk-card-media-top">
                                        <img src="img/post5.jpg" alt="" />
                                    </div>
                                    <div className="uk-card-body padding-box">
                                        <h3 className="uk-card-title">Salon 3</h3>
                                        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                            <li><a href="#">
                                                <span className="location-info">
                                                    Uttam Nagar,Delhi
                                            </span>
                                            </a></li>
                                        </ul>

                                        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                            <li><a href="#">
                                                <i className='uil uil-location-pin-alt ic-size'></i>
                                                <span className="location-info">
                                                    0.2KM
                                            </span>
                                            </a></li>
                                            <li><a href="#">
                                                <i class='uil uil-star ic-size'></i>
                                                <span className="location-info">
                                                    4.2
                                            </span>
                                            </a></li>

                                        </ul>
                                    </div>
                                    <div class="uk-card-footer book-btn">
                                        <a href="#" class="uk-button uk-button-text book-txt">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="uk-width-1-4@m">
                        <div className="uk-card uk-card-default uk-card-body">1-3</div>
                    </div>
                </div>
            </div>
        )
    }
}
