import React, { Component } from 'react'
import Male from "./Male";
import Female from "./Female";


export default class SalonProfile extends Component {
    constructor() {
        super();
        this.state = {
            status: 0,
            catlist: ''
        }
    }

    onChange(e) {
        console.log(e.target.id)
        this.setState({
            status: e.target.id
        })
        // if (this.state.status === '1') {
        //     this.setState({
        //         catlist: <Male />
        //     })
        // }
        // else if (this.state.status === '2') {
        //     this.setState({
        //         catlist: <Female />
        //     })
        // }
    }

    render() {
        return (
            <div>

                <div className="uk-visible-toggle uk-light uk-margin-bottom" tabIndex="-1" uk-slideshow="ratio: 7:2; animation: push; autoplay: true; autoplay-interval :2000	">

                    <ul className="uk-slideshow-items" style={{ minHeight: '380px' }}>
                        <li>
                            <img src="img/pinkfry.tech.png" alt="" uk-cover />
                        </li>
                        <li>
                            <img src="img/pinkfry.tech.png" alt="" uk-cover />
                        </li>
                        <li>
                            <img src="img/pinkfry.tech.png" alt="" uk-cover />
                        </li>
                    </ul>

                    <a className="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous="true" uk-slideshow-item="previous"><i class='uil uil-angle-left ic-size-img'></i></a>
                    <a className="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next="true" uk-slideshow-item="next"><i class='uil uil-angle-right ic-size-img'></i></a>

                </div>

                <div className="uk-margin-bottom bx-sh" uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar" >
                    <div class="uk-child-width-expand@s" uk-grid="true">
                        <div>
                            <div class="uk-card uk-card-default uk-card-body uk-width-1-1@m padding-box no-box-shadow">

                                <h4 className="uk-card-title detail-head">Salon 1</h4>
                                <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top uk-top">
                                    <li><a href="#">
                                        <span className="location-info-detail">
                                            NSUT,Dwarka Mor
                                            </span>
                                    </a></li>
                                    <li><a href="#">
                                        <i className='uil uil-location-pin-alt ic-size'></i>
                                        <span className="location-info-detail">
                                            0.23KM
                                            </span>
                                    </a></li>
                                    <li><a href="#">
                                        <i class='uil uil-star ic-size'></i>
                                        <span className="location-info-detail">
                                            4.2
                                            </span>
                                    </a></li>
                                </ul>


                            </div>
                        </div>
                        <div>

                            <div class="slider">
                                <div class="slide-track">
                                    <div class="slide">
                                        <img src="img/air-conditioner.png" height="45" width="45" alt="" />
                                    </div>
                                    <div class="slide">
                                        <img src="img/television.png" height="45" width="45" alt="" />
                                    </div>
                                    <div class="slide">
                                        <img src="img/couch.png" height="45" width="45" alt="" />
                                    </div>
                                    <div class="slide">
                                        <img src="img/air-conditioner.png" height="45" width="45" alt="" />
                                    </div>
                                    <div class="slide">
                                        <img src="img/credit-card.png" height="45" width="45" alt="" />
                                    </div>
                                    <div class="slide">
                                        <img src="img/milkshake.png" height="45" width="45" alt="" />
                                    </div>
                                    <div class="slide">
                                        <img src="img/parking.png" height="45" width="45" alt="" />
                                    </div>
                                    <div class="slide">
                                        <img src="img/toilet.png" height="45" width="45" alt="" />
                                    </div>
                                    <div class="slide">
                                        <img src="img/wifi.png" height="45" width="45" alt="" />
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div>
                            <div class="uk-card uk-card-default uk-card-body uk-width-1-1@m padding-box no-box-shadow" style={{ paddingTop: '30px' }}>
                                <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top uk-top">
                                    <li><a href="#">
                                        <i class='uil uil-phone ic-size-btn'></i>

                                    </a></li>
                                    <li><a href="#">
                                        <i class='uil uil-map ic-size-btn'></i>

                                    </a></li>
                                    <li><a href="#">
                                        <i class='uil uil-comment-edit ic-size-btn'></i>

                                    </a></li>
                                    <li><a href="#">
                                        <i class='uil uil-share-alt ic-size-btn'></i>

                                    </a></li>
                                </ul>


                            </div>
                        </div>

                    </div>

                </div>
                <div className="" uk-grid="true">
                    <div className="uk-width-expand@m">
                        <div className="uk-card uk-card-default uk-card-body" style={{ height: '1200px', padding: 0 }}>


                            <div class="uk-card uk-card-default uk-card-body uk-width-1-1@m padding-box no-box-shadow ">
                                <div style={{ width: '150px', margin: '0 auto' }}>
                                    <h4 className="uk-card-title uk-text-center underline">Services</h4>
                                </div>


                            </div>


                            <div className="catg uk-text-center">
                                <div className="Male " onClick={this.onChange.bind(this)}>
                                    <img src="img/male.png" className="cat-ic" id="1" />
                                </div>
                                <div className="Female ">
                                    <img src="img/female.png" className="cat-ic" id="2" onClick={this.onChange.bind(this)} />
                                </div>
                            </div>
                            <br />
                            <br />
                            <div className="uk-container">
                                {(this.state.status === '1') ? <Male /> : <Female />}
                            </div>

                        </div>

                    </div>
                    <div className="uk-width-1-3@m">
                        <div class="uk-card uk-card-default uk-card-body uk-width-1-1@m padding-box">

                            <h5 className="uk-card-title detail-head-des">Description</h5>
                            <p className="salon-desp">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis enim at, deserunt ipsum minus autem necessitatibus maxime minima consequatur tenetur?
                           </p>
                        </div>
                        <div class="uk-card uk-card-default uk-card-body uk-width-1-1@m padding-box">

                            <h5 className="uk-card-title detail-head-des">Reviews</h5>
                            <p className="salon-desp">
                                <span className="dc">"</span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis enim at, deserunt ipsum minus autem necessitatibus maxime minima consequatur tenetur?
                            </p>
                        </div>
                    </div>

                </div>

            </div>
        )
    }
}
